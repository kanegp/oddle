Task:

Create a CI/CD pipeline for a simple project (technology of their choice) that monitors the “release” and “staging” branches of the repository,
There is no restriction on the technology ( travis, gitlab, jenkins, bitbucket eg) to be used in the test.
You may clone https://github.com/johnpapa/node-hello onto your own repository.
When new commits are found, performs the following:
1. pull the branch from the repository,
2. build the application,
3. build a docker image that host your application,
4. publish the docker image on docker hub or a public cloud hosting docker repository,
5. deploys the application to a public cloud hosting (you can choose either AWS or Google Cloud Platform).
The application should show the “Hello World! {{application-version}} - {{environment}}”

Solution:

Below are the steps that I took to accomplish the task:

-	 Creating a repository on Gitlab (https://gitlab.com/kanegp/oddle) with 3 branches: “master” – “staging” and “release” with the original code cloned from (https://github.com/johnpapa/node-hello)

-	Setting up the Kubernetes cluster using Google Cloud and install the required components: Ingress and Prometheus and point a domain (soshidream.club) to the Ingress endpoint.
-	Setting up Environment Variables on GitlabCI

-	In the “staging” branch (same as release branch), add 3 new files
+ Dockerfile: to build the docker image from the source code
+ k8s.yml: a definition file to deploy the image created on build step to Kubernetes cluster
+ .gitlab-ci.yml: the declarative file to automate the CI and CD process
-	Modify the index.js file to use specific pattern, and then using “sed” to pass the information to the display homepage after each build:
const msg = 'Hello Staging! This is commit number "xxx" for "xxx" environment\n'

-	Everytime the dev team commits the code to the “staging” branch, Gitlab will trigger the pipelines and we can see the changes at: http://soshidream.club/staging ; same as “release” branch at http://soshidream.club/release 
